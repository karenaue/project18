from aux_notebook import *
import requests
import io


    
url = "https://raw.githubusercontent.com/anonymous-replication/replication-nasa/master/data/processed/CM1-processed.csv" 
download = requests.get(url).content
NASA_CM1 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/anonymous-replication/replication-nasa/master/data/processed/KC3-processed.csv" 
download = requests.get(url).content
NASA_KC3 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/anonymous-replication/replication-nasa/master/data/processed/MC2-processed.csv" 
download = requests.get(url).content
NASA_MC2 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/anonymous-replication/replication-nasa/master/data/processed/MW1-processed.csv" 
download = requests.get(url).content
NASA_MW1 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/anonymous-replication/replication-nasa/master/data/processed/PC1-processed.csv" 
download = requests.get(url).content
NASA_PC1 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/anonymous-replication/replication-nasa/master/data/processed/PC2-processed.csv" 
download = requests.get(url).content
NASA_PC2 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/anonymous-replication/replication-nasa/master/data/processed/PC3-processed.csv" 
download = requests.get(url).content
NASA_PC3 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/anonymous-replication/replication-nasa/master/data/processed/PC4-processed.csv" 
download = requests.get(url).content
NASA_PC4 = pd.read_csv(io.StringIO(download.decode('utf-8')))

numerical_response(NASA_CM1, 'NASA') 
numerical_response(NASA_KC3, 'NASA') 
numerical_response(NASA_MC2, 'NASA') 
numerical_response(NASA_MW1, 'NASA') 
numerical_response(NASA_PC1, 'NASA') 
numerical_response(NASA_PC2, 'NASA') 
numerical_response(NASA_PC3, 'NASA') 
numerical_response(NASA_PC4, 'NASA') 


project_dict_NASA = {
  "NASA_CM1 ": NASA_CM1,
  "NASA_KC3 ": NASA_KC3,
  "NASA_MC2 ": NASA_MC2, 
  "NASA_MW1 ": NASA_MW1, 
  "NASA_PC1 ": NASA_PC1,
  "NASA_PC2 ": NASA_PC2,
  "NASA_PC3 ": NASA_PC3,
  "NASA_PC4 ": NASA_PC4}

drop_list = ["Unnamed: 0"]
drop_variables(NASA_CM1, drop_list)
drop_variables(NASA_KC3, drop_list)
drop_variables(NASA_MC2, drop_list)
drop_variables(NASA_MW1, drop_list)
drop_variables(NASA_PC1, drop_list)
drop_variables(NASA_PC2, drop_list)
drop_variables(NASA_PC3, drop_list)
drop_variables(NASA_PC4, drop_list)