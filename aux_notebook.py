#Libraries and packages:

import numpy as np
import pandas as pd
import seaborn as sns
import scipy as sp

import requests
import io

from numpy import linalg as la
from numpy import random

from matplotlib import pyplot as plt

from sklearn import preprocessing
from sklearn.metrics import roc_auc_score
from sklearn.feature_selection import RFE
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import ComplementNB
from sklearn.metrics import confusion_matrix
from sklearn.naive_bayes import MultinomialNB
from sklearn.cluster import SpectralClustering
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_classification
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import average_precision_score
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_circles, make_blobs
from sklearn.feature_selection import VarianceThreshold
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.feature_selection import SequentialFeatureSelector
from sklearn.naive_bayes import BernoulliNB # ment for binary features ... 

from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import RandomOverSampler

from scipy.io.arff import loadarff 
from scipy.spatial.distance import euclidean, pdist, squareform


#Auxillary function to make our data readable:

def numerical_response(df, data_set):
    if data_set == 'AEEEM':        
        for i in range(len(df["class"])):
            clean_or_buggy = str(df["class"].iloc[i])
            if  clean_or_buggy[2:7] == 'clean':
                df["class"].iloc[i] = 0
            else: 
                df["class"].iloc[i] = 1

    if data_set == 'NASA':
        df.rename(columns={'Defective':'class'}, inplace=True)
        df.loc[df["class"] == 'Y', "class"] = 1
        df.loc[df["class"] == 'N', "class"] = 0
        
        
    if data_set == 'PROMISE':
        df.rename(columns={'bug':'class'}, inplace=True)
        df.loc[df["class"] > 0, "class"] = 1
          
    return 

#Functions to help investigate data:

def confmatrix(df_train,df_test,model):
    Y_pred, Y_test = model(df_train,df_test)
    conf = confusion_matrix(Y_test, Y_pred)
    TN, FP, FN, TP = confusion_matrix(Y_test, Y_pred).ravel()
    

    labels = ["True Neg", "False Pos", "False Neg", "True Pos"]
    labels = np.asarray(labels).reshape(2,2)
    allvalues = TN+TP+FN+FP
    print("Total Non-defect in test data", TN+FP, "Specificity: ", round(TN/(TN+FP),2)*100 )
    print("Total Defect in test data", FN+TP, "Sensitivity: ", round(TP/(TP+FN),2)*100)
    print("True Positives: ", TP, "Percent: ", round(TP/allvalues,2)*100)
    print("False Negatives: ", FN, "Percent: ", round(FN/allvalues,2)*100)
    print("True Negatives: ", TN, "Percent: ", round(TN/allvalues,2)*100)
    print("False Positives: ", FP, "Percent: ", round(FP/allvalues,2)*100)
    sns.heatmap(conf/np.sum(conf), annot = True, fmt='.2%', cmap = 'binary')
    return


def investigate_df(df):
    data_points = len(df["class"])
    buggy = df["class"].sum()
    percent_buggy = round((buggy / data_points )* 100, 1) 
    print('Name of project', df)
    print('Number of data points = ',data_points)
    print('Number of buggy data points = ',buggy)
    print('Percent buggy data points = ', percent_buggy, '%') 
    return 


def plot_two_columns(df, col_name1, col_name2):
    plt.subplot(1, 2, 2)
    plt.hist2d(df[col_name1], df[col_name2], cmap=plt.cm.YlGn)
    plt.colorbar()
    plt.xlabel(col_name1)
    
    plt.subplot(1, 2,1)
    plt.scatter(df[col_name1], df[col_name2])
    plt.xlabel(col_name1)
    plt.ylabel(col_name2)
    plt.show()
    return



def plot_var(df):
    col_names = (df.columns).tolist()
    for i in range(len(col_names)):
        plot_two_columns(df, 'class', col_names[i])
    return



def pearsoncor(df): #Return a heatmap of pairwise correlation among all columns in a dataframe.
    sns.heatmap(df.corr())
    return

def plot_two_columns(df, col_name1, col_name2):
    plt.subplot(1, 2, 2)
    plt.hist2d(df[col_name1], df[col_name2], cmap=plt.cm.YlGn)
    plt.colorbar()
    plt.xlabel(col_name1)
    plt.ylabel(col_name2)
    
    plt.subplot(1, 2,1)
    plt.scatter(df[col_name1], df[col_name2])
    plt.xlabel(col_name1)
    plt.ylabel(col_name2)
    plt.show()
    return


def histofdf(df):  #Create a histogram of all columns in a dataframe.
    df.hist(bins=20, 
          figsize=(25, 55),
          xlabelsize = 12, 
          grid = False, 
          linewidth=3.0,
          layout = (16,4))
    return



def find_correlated_columns(df, treshold): #Find columns in a project with correlation > X
    correlated_columns = []
    cor = df.corr()
    np.fill_diagonal(cor.values, 0)
    column_names = (cor.columns).to_list()
    i = 0
    current_length = len(column_names) - 1
    while i < current_length:
        max_value = max(abs(cor.iloc[i,:]))
        if max_value > treshold:
            correlated_columns.append(column_names[i])
            cor.drop(columns=column_names[i], inplace=True)
            current_length = current_length - 1

        i = i + 1 
    return correlated_columns


def drop_variables(df, drop_list): #Drop specific features of a project to decrease dimensionality.
    df.drop(columns=drop_list, inplace=True)
    return

"""# **Functions to help tune the models:**"""

def resample(df):
    df_copy = df.copy()
    X, y = df_copy.iloc[:,:-1], df_copy.iloc[:,-1]
    y=y.astype('int')
    ros = RandomOverSampler(random_state=7)
    X_res, y_res = ros.fit_resample(X, y)
    df_res = pd.concat([X_res, y_res], axis=1)
    return df_res
    
'''
def sfs(model, X_train, Y_train, n_features):
    
    sfs = SequentialFeatureSelector(model, n_features_to_select=n_features, scoring = "roc_auc", )
    sfs.fit(X_train, Y_train)
    boolean_for_X = sfs.get_support()
    return boolean_for_X
'''

def backward_feature_selection_unsupervised(df_train, func, scale = False, log = False):
    # returns a drop list with variables we should drop
    columns = df_train.columns.to_list()
    number_of_columns = len(columns)
    number_of_variables = len(columns) -1
    auc_list = []
    dropped_column_list = []

    y = list(df_train['class'])
    
    X_train = df_train.iloc[:,:-1]
    
    if scale == True:
        X_train = scale_data_unsupervised(df_train)
        
    if log == True:
        X_train = logX(df_train)
    
    X_train_copy = (X_train).copy()
    
    for j in range(number_of_columns - 2):
        auc_temp_list = []
        columns = X_train_copy.columns.to_list()
        
        number_of_columns = len(columns)
        print(j," iteration done: ", number_of_columns - 2, " iterations left.")

        for i in range(number_of_columns - 1):

            X_train = X_train_copy.copy()
            X_train.drop(columns = columns[i], inplace=True)
            model = func()
            y_pred = model.fit_predict(X_train)
            auc_temp_list.append(roc_auc_score(y, y_pred))
        
        idx = np.argmax(auc_temp_list)
        auc_list.append(auc_temp_list[idx])
        dropped_column_list.append(columns[idx])
        print("Removed ", columns[idx])
        print("Shape of data frame: ",np.shape(X_train_copy),"\n")
        X_train_copy.drop(columns = columns[idx], inplace=True)
    
    idx_max_auc = np.argmax(auc_list)
    print("\nMax auc value: ",np.round(auc_list[idx_max_auc], 2))
    print("and is found using ", number_of_variables-idx_max_auc ," variables.")
        
    return dropped_column_list[0:idx_max_auc]


def backward_feature_selection(df_train, df_test, func, scale = False, log = False):
    # returns a drop list with variables we should drop
    columns = df_train.columns.to_list()
    number_of_columns = len(columns)
    number_of_variables = len(columns) -1
    auc_list = []
    dropped_column_list = []

    y_train = list(df_train['class'])
    y_test = list(df_test['class'])
    
    X_train = df_train.iloc[:,:-1]
    X_test = df_test.iloc[:,:-1]
    
    if scale == True:
        X_train, X_test = scale_data(df_train, df_test)
        
    if log == True:
        X_train = logX(df_train)
        X_test = logX(df_test)
    
    X_train_copy = (X_train).copy()
    X_test_copy = (X_test).copy()
    
    for j in range(number_of_columns - 2):
        auc_temp_list = []
        columns = X_train_copy.columns.to_list()
        
        number_of_columns = len(columns)
        print(j," iteration done: ", number_of_columns - 2, " iterations left.")

        for i in range(number_of_columns - 1):

            X_train = X_train_copy.copy()
            X_test = X_test_copy.copy()
            X_train.drop(columns = columns[i], inplace=True)
            X_test.drop(columns = columns[i], inplace=True)
            
            model = func()
            model.fit(X_train, y_train)
            
            y_pred = model.predict(X_test)
            auc_temp_list.append(roc_auc_score(y_test, y_pred))
        
        idx = np.argmax(auc_temp_list)
        auc_list.append(auc_temp_list[idx])
        dropped_column_list.append(columns[idx])
        print("Removed ", columns[idx])
        print("Shape of data frame: ",np.shape(X_train_copy),"\n")
        X_train_copy.drop(columns = columns[idx], inplace=True)
        X_test_copy.drop(columns = columns[idx], inplace=True)
    
    idx_max_auc = np.argmax(auc_list)
    print("\nMax auc value: ",np.round(auc_list[idx_max_auc], 2))
    print("and is found using ", number_of_variables-idx_max_auc ," variables.")
        
    return dropped_column_list[0:idx_max_auc]



def scale_data_unsupervised(df):
    df_copy = df.copy() 
    std_scaler = StandardScaler()
    df_std = pd.DataFrame(std_scaler.fit_transform(df_copy), columns=df_copy.columns)
    
    return df_std



def scale_data(train_data, test_data):
    X_train = (train_data.iloc[:,:-1]).copy()
    X_test = (test_data.iloc[:,:-1]).copy()
    
    std_scaler = StandardScaler()
    
    scaled_train_X = pd.DataFrame(std_scaler.fit_transform(X_train), columns=X_train.columns)
    scaled_test_X = pd.DataFrame(std_scaler.fit_transform(X_test), columns=X_test.columns)

    return scaled_train_X, scaled_test_X



def minmax(df_train, df_test):
    X_train = df_train.copy()
    X_train = X_train.iloc[:,:-1]
    X_test = df_test.copy()
    X_test = X_test.iloc[:,:-1]   
    
    scaler = MinMaxScaler(feature_range=(0, 3))
    
    #Apply the function
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.fit_transform(X_test)
    
    #Convert to dataframes
    X_train = pd.DataFrame(X_train)
    X_test = pd.DataFrame(X_test)
    return X_train, X_test

def minmax_unsupervised(df_train):
    X_train = df_train.copy()
    X_train = X_train.iloc[:,:-1]
      
    scaler = MinMaxScaler(feature_range=(0, 3))
    
    #Apply the function
    X_train = scaler.fit_transform(X_train)
    
    #Convert to dataframes
    X_train = pd.DataFrame(X_train)
    return X_train


def logX(df):
    df_copy = df.copy() 
    df_copy += 1
    X = (df_copy.iloc[:,:-1]).copy()
    length = len(X.columns.to_list())
    for i in range(length):
        X.iloc[:,i] = np.log(X.iloc[:,i])        
    return X


"""# **Performance of the models**"""

def AUC_unsupervised(function, df, project_list = 0):
    y_pred, y_test = function(df)
    auc = roc_auc_score(y_test, y_pred)
    
    return np.round(np.mean(auc), 2)
    
    
    
def average_AUC_cross(function, df_test, projects = 0):
    n = len(projects)
    auc = np.zeros(n)
    for j in range(n):
        df_train = projects[j]
        y_pred, y_test = function(df_train, df_test)
        auc[j] = roc_auc_score(y_test, y_pred)
        
    return np.round(np.mean(auc), 2)



def average_AUC_within(function, df, project_list = 0):
    t = 500 # change to 500 for final results
    auc = np.zeros(2*t)
    for i in range(t):
        #First split within:
        x_train, x_test, y_train, y_test = train_test_split(df.iloc[:,:-1], df.iloc[:,-1], test_size=0.3, shuffle=True)
        
        #Create a training dataframe
        x_train = pd.DataFrame(x_train)
        y_train = pd.DataFrame(y_train)   
        df1 = pd.concat([x_train, y_train], axis=1)
        #Create a test dataframe
        x_test = pd.DataFrame(x_test)
        y_test = pd.DataFrame(y_test)   
        df2 = pd.concat([x_test, y_test], axis=1)
        
        y_pred, y_test = function(df1, df2)
        auc[i] = roc_auc_score(y_test, y_pred)
        '''
        #Second split within:
        x_train, x_test, y_train, y_test = train_test_split(df.iloc[:,:-1], df.iloc[:,-1], test_size=0.3, shuffle=True)
        
        #Create a training dataframe
        x_train = pd.DataFrame(x_train)
        y_train = pd.DataFrame(y_train)   
        df1 = pd.concat([x_train, y_train], axis=1)
        #Create a test dataframe
        x_test = pd.DataFrame(x_test)
        y_test = pd.DataFrame(y_test)   
        df2 = pd.concat([x_test, y_test], axis=1)        
 
        y_pred, y_test = function(df1, df2)
        auc[2*t - i - 1] = roc_auc_score(y_test, y_pred)
        '''
        
    return np.round(np.mean(auc), 2)



def get_auc_scores(project_dict, func, auc_func):
    average_auc_list = []
    
    project_list = list(project_dict.values())
    project_names = list(project_dict.keys())
    
    all_projects = project_list

    for p in range(len(project_list)): 
        # only for cross project        
        train_project = all_projects.copy()
        train_project.pop(p)
        
        # for all (cross project, within project and unsupervised)
        test_project = project_list[p]

        auc = auc_func(func, test_project, train_project)
        print('\nAUC score for ' + project_names[p] + ' :', auc)
        average_auc_list.append(auc)
    
    print('\nMean AUC score for all projects:', np.round(np.mean(average_auc_list),2))  
    return average_auc_list


def scott_knott(function, df):
    t = 500 # change to 500 for final results
    auc = np.zeros(2*t)
    for i in range(t):
        df1, df2 = train_test_split(df, test_size=0.5)
        y_pred, y_test = function(df1, df2)
        auc[i] = roc_auc_score(y_test, y_pred)
        y_pred, y_test = function(df2, df1)
        auc[2*t - i - 1] = roc_auc_score(y_test, y_pred)
    print(len(auc))
    
    return auc

"""# **To present results:**"""

def to_csv_supervised(project_dict, function_dict, file_name):
    measure = ['CP', 'WP', 'diff']
    m = len(measure)
    function_names = list(function_dict.keys())
    column_names =[]
    for i in range(len(function_dict)):
        for j in range(m):
            column_names.append(function_names[i] + '\n' + measure[j])
    project_list = list(project_dict.values())
    function_list = list(function_dict.values())
    row_names = list(project_dict.keys())
    
    col = 0
    all_projects = project_list
    
    # initialize data
    data = np.zeros((len(project_list), len(function_list)*m))
    for func in function_list:
        print(func) # del
        for p in range(len(project_list)):         
            train_project = all_projects.copy()
            train_project.pop(p)
            
            test_project = project_list[p]
            
            AUC_cross = average_AUC_cross(func, test_project, train_project)
            AUC_within = average_AUC_within(func, test_project)
            
            data[p,col] = AUC_cross
            data[p,col+1] = AUC_within
            data[p,col+2] = round(AUC_within - AUC_cross,2)
            print(project_list[p])           
        col = col + m
    df = pd.DataFrame(data, index = row_names) 
    df.to_csv(file_name, encoding='utf-8', index=True, header=column_names)
    return



def to_csv_unsupervised(project_dict, function_dict, file_name):
    function_names = list(function_dict.keys())
    column_names =[]
    for i in range(len(function_dict)):
        column_names.append(function_names[i])
    
    project_list = list(project_dict.values())
    function_list = list(function_dict.values())
    row_names = list(project_dict.keys())
    
    col = 0
    all_projects = project_list
    
    # initialize data
    data = np.zeros((len(project_list), len(function_list)))
    for func in function_list:
        for p in range(len(project_list)):         
            df = project_list[p]
    
            AUC = AUC_unsupervised(func, df)
            data[p,col] = AUC
            
    df = pd.DataFrame(data, index = row_names) 
    df.to_csv(file_name, encoding='utf-8', index=True, header=column_names)
    return


'''
#"Optimizations" that did not add value to our models:


#From random forrest:

    #Recursive Feature Elimination to find the best possible model. We use Logistic Regression model to find it.
    model = LogisticRegression(max_iter = 10000)
    rfe = RFE(model, n_features_to_select = 15)
    fit = rfe.fit(X_train, y_train)
    bll = fit.support_
    X_train_RFE = X_train.loc[:,bll]
    X_test_RFE = X_test.loc[:,bll]
    #Apply the features found above in the data
    X_train = X_train_RFE
    X_test = X_test_RFE 
    #Apply minmax to decrease computation time.
    X_train, X_test = minmax(X_train, X_test)

#From random forrest:

    #Sequential Feature Selection
    BLL = sfs(model, X_train, y_train, 15)
    X_train = X_train.loc[:,BLL]
    X_test = X_test.loc[:,BLL]

#From logistic regression:
  
    #This section can use Recursive Feature Elimination to find the best model.
    # It does not improve the model so it is omitted.
    #USE RFE to pick features.
    model = LogisticRegression(max_iter = 10000)
    rfe = RFE(model, n_features_to_select = 15)
    fit = rfe.fit(X_train, y_train)
    bll = fit.support_
    X_train = X_train.loc[:,bll]
    X_test = X_test.loc[:,bll]
        

#From K means

    #USE RFE to pick features.
    model = KMeans(max_iter = 10000)
    rfe = RFE(model, 20)
    fit = rfe.fit(scaled_train_X, y_train)
    bll = fit.support_
    scaled_train_X = scaled_train_X.loc[:,bll]


'''

