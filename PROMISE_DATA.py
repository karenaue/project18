from aux_notebook import *
import requests
import io



url = "https://raw.githubusercontent.com/feiwww/PROMISE-backup/master/bug-data/camel/camel-1.6.csv" 
download = requests.get(url).content
PROMISE_Camel_v_1_6 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/feiwww/PROMISE-backup/master/bug-data/ivy/ivy-1.2.csv" 
download = requests.get(url).content
PROMISE_IVY_1_2 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/feiwww/PROMISE-backup/master/bug-data/jedit/jedit-4.0.csv" 
download = requests.get(url).content
PROMISE_Jedit_v_4_0 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/feiwww/PROMISE-backup/master/bug-data/log4j/log4j-1.0.csv" 
download = requests.get(url).content
PROMISE_Log4j_v_1_0 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/feiwww/PROMISE-backup/master/bug-data/lucene/lucene-2.4.csv" 
download = requests.get(url).content
PROMISE_Lucene_v_2_4 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/feiwww/PROMISE-backup/master/bug-data/poi/poi-3.0.csv" 
download = requests.get(url).content
PROMISE_POI_v_3_0 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/feiwww/PROMISE-backup/master/bug-data/xalan/xalan-2.6.csv" 
download = requests.get(url).content
PROMISE_Xalan_v_2_6 = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/feiwww/PROMISE-backup/master/bug-data/xerces/xerces-1.3.csv" 
download = requests.get(url).content
PROMISE_Xerces_v_1_3 = pd.read_csv(io.StringIO(download.decode('utf-8')))


numerical_response(PROMISE_Camel_v_1_6, 'PROMISE')
numerical_response(PROMISE_IVY_1_2, 'PROMISE')
numerical_response(PROMISE_Jedit_v_4_0, 'PROMISE')
numerical_response(PROMISE_Log4j_v_1_0, 'PROMISE') 
numerical_response(PROMISE_Lucene_v_2_4, 'PROMISE')
numerical_response(PROMISE_POI_v_3_0, 'PROMISE')
numerical_response(PROMISE_Xalan_v_2_6, 'PROMISE') 
numerical_response(PROMISE_Xerces_v_1_3, 'PROMISE') 

project_dict_PROMISE = {
  "PROMISE_Camel_v_1_6 ": PROMISE_Camel_v_1_6,
  "PROMISE_IVY_1_2 ": PROMISE_IVY_1_2,
  "PROMISE_Jedit_v_4_0 ": PROMISE_Jedit_v_4_0,
  "PROMISE_Log4j_v_1_0 ": PROMISE_Log4j_v_1_0,
  "PROMISE_Lucene_v_2_4 ": PROMISE_Lucene_v_2_4,
  "PROMISE_POI_v_3_0 ": PROMISE_POI_v_3_0,
  "PROMISE_Xalan_v_2_6 ": PROMISE_Xalan_v_2_6,
  "PROMISE_Xerces_v_1_3 ": PROMISE_Xerces_v_1_3}

drop_list = ["name"]
drop_variables(PROMISE_Camel_v_1_6, drop_list)
drop_variables(PROMISE_IVY_1_2, drop_list)
drop_variables(PROMISE_Jedit_v_4_0, drop_list)
drop_variables(PROMISE_Log4j_v_1_0, drop_list)
drop_variables(PROMISE_Lucene_v_2_4, drop_list)
drop_variables(PROMISE_POI_v_3_0, drop_list)
drop_variables(PROMISE_Xalan_v_2_6, drop_list)
drop_variables(PROMISE_Xerces_v_1_3, drop_list)