from aux_notebook import *
import requests
import io

url = "https://raw.githubusercontent.com/Kasperbang/AEEEM/main/AEEEM_EQ.csv" 
download = requests.get(url).content
AEEEM_EQ = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/Kasperbang/AEEEM/main/AEEEM_JDT.csv" 
download = requests.get(url).content
AEEEM_JDT = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/Kasperbang/AEEEM/main/AEEEM_LC.csv" 
download = requests.get(url).content
AEEEM_LC = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/Kasperbang/AEEEM/main/AEEEM_ML.csv" 
download = requests.get(url).content
AEEEM_ML = pd.read_csv(io.StringIO(download.decode('utf-8')))

url = "https://raw.githubusercontent.com/Kasperbang/AEEEM/main/AEEEM_PDE.csv" 
download = requests.get(url).content
AEEEM_PDE = pd.read_csv(io.StringIO(download.decode('utf-8')))

numerical_response(AEEEM_JDT, 'AEEEM')
numerical_response(AEEEM_EQ, 'AEEEM')
numerical_response(AEEEM_LC, 'AEEEM')
numerical_response(AEEEM_ML, 'AEEEM')
numerical_response(AEEEM_PDE, 'AEEEM') 

project_dict_AEEEM = {
  "Eclipse JDT Core, AEEEM_JDT  ": AEEEM_JDT,
  "Equinox, AEEEM_EQ ": AEEEM_EQ,
  "Apache Lucene , AEEEM_LC ": AEEEM_LC,
  "Mylyn, AEEEM_ML" : AEEEM_ML,
  "Eclipse PDE UI, AEEEM_PDE ": AEEEM_PDE}

drop_list = ["Unnamed: 0"]
drop_variables(AEEEM_JDT, drop_list)
drop_variables(AEEEM_EQ, drop_list)
drop_variables(AEEEM_LC, drop_list)
drop_variables(AEEEM_ML, drop_list)
drop_variables(AEEEM_PDE, drop_list)
